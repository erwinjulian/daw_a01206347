-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 27-10-2017 a las 20:59:24
-- Versión del servidor: 5.5.57-0ubuntu0.14.04.1
-- Versión de PHP: 5.5.9-1ubuntu4.22

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `BDpetz`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `zombis`
--
-- Creación: 27-10-2017 a las 18:40:05
--

DROP TABLE IF EXISTS `zombis`;
CREATE TABLE IF NOT EXISTS `zombis` (
  `nombre` varchar(100) NOT NULL,
  `estado` varchar(100) NOT NULL,
  `fecharegistro` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `fechaestado` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`fecharegistro`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Truncar tablas antes de insertar `zombis`
--

TRUNCATE TABLE `zombis`;
--
-- Volcado de datos para la tabla `zombis`
--

INSERT DELAYED IGNORE INTO `zombis` (`nombre`, `estado`, `fecharegistro`, `fechaestado`) VALUES
('Rocío', 'completamente muerto', '2017-10-01 00:00:00', '2017-10-23 00:28:00'),
('Erwin', 'infección', '2017-10-01 04:00:00', '2017-10-13 00:05:00'),
('Octavinder', 'transformación', '2017-10-14 00:00:00', '2017-10-10 00:05:13'),
('Juan', '2', '2017-10-27 05:17:24', '2017-10-27 05:17:25'),
('Pedro', 'infección', '2017-10-27 19:40:53', '2017-10-27 05:17:25'),
('Pedro', 'completamente muerto', '2017-10-27 19:55:56', '2017-10-16 00:00:00'),
('Iñaqui', 'coma', '2017-10-27 19:56:40', '2017-10-27 12:14:07'),
('Hockey', 'coma', '2017-10-27 19:57:33', '2017-10-25 14:21:00'),
('Ricardo', 'infección', '2017-10-27 19:58:27', '2017-10-01 00:00:00'),
('zombi sin nombre', 'infección', '2017-10-27 19:59:13', '2017-10-04 00:00:00');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

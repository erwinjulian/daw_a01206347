<?php

		function conecta(){
			$mysql = mysqli_connect("localhost","root","","BDpetz");
			$mysql->set_charset("utf8");
			return $mysql;
		}

		function desconecta($mysql){
			mysqli_close($mysql);
		}

		function selectZombis(){
			$db = conecta();
			$query = 'SELECT * FROM zombis';
			$result = mysqli_query($db,$query);
			desconecta($db);
			return $result;
		}

		function showZombis(){
			$result = selectZombis();
			echo "<table class=bordered>";
			echo "<tr>";
			echo "<th>Nombre</th>";
			echo "<th>Estado</th>";
			echo "<th>Fecha y hora de Registro</th>";
			echo "<th>Fecha y hora a su estado actual</th>";
			echo "</tr>";
			while($fila = mysqli_fetch_array($result,MYSQLI_BOTH)){
				echo "<tr>";
				echo "<td>" . $fila["nombre"] . "</td>";
				echo "<td>" . $fila["estado"] . "</td>";
				echo "<td>" . $fila["fecharegistro"] . "</td>";
				echo "<td>" . $fila["fechaEstado"] . "</td>";
				echo "<td><a href=\"EditarZombi.php?mat=$fila[fecharegistro]\" class=\"btn-floating btn-large waves-effect waves-light blue-1\"><i class=\"material-icons\">editar</i></a></td>";
				echo "</tr>";
			}
			echo "</table>";
		}

		function guardarZombis($nombre,$estado,$fecharegistro,$fechaestado){
		$db = conecta();

		$query = 'INSERT INTO zombis (nombre, estado, fecha, fecha) VALUES (?,?,?,?)';

		if (!($statement = $db->prepare($query))) {
	    die("Preparation failed: (" . $db->errno . ") " . $db->error);
	    }
	    // Binding statement http_parse_params(

	    if (!$statement->bind_param("ss", $nombre, $estado, $fecha, $fecha)){
	        die("Parameter vinculation failed: (" . $statement->errno . ") " . $statement->error); 
	    }

	    // Executing the statement
	    if (!$statement->execute()) {
	        die("Execution failed: (" . $statement->errno . ") " . $statement->error);
	     }
		}
		
			function editarZombis($nombre,$estado,$fecha){
		$db = conecta();

		$query="UPDATE zombis SET nombre=?,estado=?,fecha=? WHERE correo LIKE ? ";

	    if (!($statement = $db->prepare($query))) {
	    	die("Preparation failed: (" . $db->errno . ") " . $db->error);
	    }
	    // Binding statement http_parse_params(	

	    if (!$statement->bind_param("sssssssis", $nombre,$estado,$fecha)) {
	        die("Parameter vinculation failed: (" . $statement->errno . ") " . $statement->error); 
	    }
	    // Executing the statement
	    if (!$statement->execute()) {
	        die("Execution failed: (" . $statement->errno . ") " . $statement->error);
	     } 

		desconecta($db);
		
		function showEstado(){
		$result=selectEstado();
		$estado = ['infección', 'completamente muerto','coma', 'transformación'];
		echo "<table class=bordered>";
		echo "<tr>";
		echo "<th>Nombre Completo</th>";
		echo "<th>Estado Actual</th>";
		echo "<th>Fecha de Registro</th>";
		echo "<th>Fecha de EstadoActual</th>";
		echo "</tr>";
		while($fila = mysqli_fetch_array($result,MYSQLI_BOTH)){
			echo "<tr>";
			echo "<td>" . $fila["nombre"] . " " .  $fila["estado"] . "</td>";
			echo "<td>" . $fila["fecharegistro"] . "</td>";
			$date = new DateTime( $fila["fecha"]);
			$fecha = $date->format('d/M/Y');
			$fecha = str_replace('Jan','Ene',$fecha);
			$fecha = str_replace('Apr','Abr',$fecha);
			$fecha = str_replace('Aug','Ago',$fecha);
			$fecha = str_replace('Dec','Dic',$fecha);
			echo "<td>" . $fecharegistro . "</td>";
			echo "<td><a href=editarZombis.php?correo=$fila[fecharegistro] class=\"btn-floating btn-large waves-effect waves-light blue darken-1\"><i class=material-icons>edit</i></a></td>";
			echo "</tr>";
		}
		echo "</table>";
		echo "<br><br>";
	}
			}
			
	function getZombi($db,$correo){
		$query = "SELECT * FROM voluntario WHERE correo LIKE '$correo'";
		$result = mysqli_query($db,$query);
		$fila = mysqli_fetch_array($result,MYSQLI_BOTH);
		return $fila;
	}

?>
SELECT *
FROM materiales

select * from materiales 
where clave=1000 

select clave,rfc,fecha from entregan 

select * from materiales,entregan 
where materiales.clave = entregan.clave 

select * from entregan e, proyectos p
where e.numero < = p.numero 

(select * from entregan where clave=1450)
union 
(select * from entregan where clave=1300) 

SELECT *
FROM Entregan
WHERE clave=1300
OR clave=1450

(select clave from entregan where numero=5001) 
intersect 
(select clave from entregan where numero=5018)

SELECT *
FROM Entregan
WHERE clave != 1000

select * from entregan,materiales 

SELECT descripcion
FROM Materiales M, Entregan E
WHERE M.clave = E.clave
AND fecha > '31-DEC-1999'
AND fecha < '01-JAN-2001'

SET DATEFORMAT dmy
select m.Descripcion from materiales m, entregan e
where m.Clave = e.Clave AND e.Fecha BETWEEN '01/01/2000' AND '31/12/2000'

SELECT DISTINCT descripcion
FROM Materiales M, Entregan E
WHERE M.clave = E.clave
AND fecha > '31-DEC-1999'
AND fecha < '01-JAN-2001'

SELECT P.numero, denominacion, fecha, cantidad
FROM Proyectos P, Entregan E
WHERE P.numero = E.numero
ORDER BY P.numero, fecha DESC

SELECT * FROM Materiales where Descripcion LIKE 'Si%' 

DECLARE @foo varchar(40); 
DECLARE @bar varchar(40); 
SET @foo = '�Que resultado'; 
SET @bar = ' ���??? ' 
SET @foo += ' obtienes?'; 
PRINT @foo + @bar; 

SELECT RFC FROM Entregan WHERE RFC LIKE '[A-D]%'; 
SELECT RFC FROM Entregan WHERE RFC LIKE '[^A]%'; 
SELECT Numero FROM Entregan WHERE Numero LIKE '___6'; 

SELECT Clave,RFC,Numero,Fecha,Cantidad 
FROM Entregan 
WHERE Numero Between 5000 and 5010; 

SET DATEFORMAT dmy
SELECT *
FROM Entregan 
WHERE numero BETWEEN 5000 AND 5010
AND fecha BETWEEN '01/01/2000' AND '31/12/2000'

SELECT RFC,Cantidad, Fecha,Numero 
FROM [Entregan] 
WHERE [Numero] BETWEEN 5000 AND 5010 
AND EXISTS ( SELECT [RFC] 
			 FROM [Proveedores] 
			 WHERE RazonSocial LIKE 'La%' 
			 AND [Entregan].[RFC] = [Proveedores].[RFC] )
			 
SELECT RFC,Cantidad, Fecha,Numero 
FROM [Entregan] 
WHERE [Numero] BETWEEN 5000 AND 5010 
AND RFC IN ( SELECT [RFC], RazonSocial
			 FROM [Proveedores] 
			 WHERE RazonSocial LIKE 'La%' 
			 AND [Entregan].[RFC] = [Proveedores].[RFC] )

SELECT RFC,Cantidad, Fecha,Numero 
FROM [Entregan] 
WHERE [Numero] BETWEEN 5000 AND 5010 
AND RFC NOT IN ( SELECT [RFC]
			     FROM [Proveedores] 
			     WHERE RazonSocial NOT LIKE 'La%' 
			     AND [Entregan].[RFC] = [Proveedores].[RFC] )

SELECT RFC, Cantidad, Fecha, Numero 
from entregan
where Numero = ANY (select Numero
					from proyectos
					where denominacion LIKE 'Vamos%')

SELECT TOP 2 * FROM Proyectos

SELECT TOP Numero FROM Proyectos 

ALTER TABLE materiales ADD PorcentajeImpuesto NUMERIC(6,2);

UPDATE materiales SET PorcentajeImpuesto = 2*clave/1000; 

SELECT * FROM Materiales

SELECT E.Cantidad, M.Costo, M.PorcentajeImpuesto, (Cantidad*Costo)*(1+PorcentajeImpuesto/100) as Importe
FROM Materiales M, Entregan E
WHERE M.Clave = E.Clave

SELECT m.clave, descripcion
FROM Materiales M, Entregan E, Proyectos P
WHERE M.clave = E.Clave
AND P.numero = E.numero
AND denominacion LIKE 'Mexico sin ti no estamos completos'

SELECT m.clave, descripcion
FROM Materiales M, Entregan E, Proveedores P
WHERE M.clave = E.Clave
AND P.rfc = E.rfc
AND RazonSocial LIKE 'Acme tools'

SET DATEFORMAT dmy
SELECT P.rfc /*AVG(cantidad) as Promedio*/
FROM Proveedores P, Entregan E
WHERE P.rfc = E.rfc
AND fecha BETWEEN '01/01/2000' AND '31/12/2000'
GROUP BY P.rfc
HAVING AVG(cantidad)>300

SET DATEFORMAT dmy
SELECT descripcion, SUM(cantidad) as Total
FROM Materiales M, Entregan E
WHERE M.clave = E.clave
AND fecha BETWEEN '01/01/2000' AND '31/12/2000'
GROUP BY descripcion

SET DATEFORMAT dmy
CREATE VIEW Materiales2001 AS
(
	SELECT Top 1 descripcion, M.clave, SUM(cantidad) as Total
	FROM Materiales M, Entregan E
	WHERE M.clave = E.clave
	AND fecha BETWEEN '01/01/2001' AND '31/12/2001'
	GROUP BY descripcion, M.clave
	ORDER BY Total DESC
)

SELECT clave
FROM Materiales2001

DROP VIEW  Materiales2001

SELECT *
FROM Materiales
WHERE descripcion  LIKE '%ub%'


SELECT denominacion,  SUM(Cantidad*Costo) as Total
FROM Materiales M, Entregan E, Proyectos P
WHERE M.clave=E.clave
AND E.numero=P.numero
GROUP BY denominacion

CREATE VIEW Televisa AS
(
	SELECT denominacion, E.rfc, razonsocial
	FROM Proyectos P, Entregan E, Proveedores Pr
	WHERE P.numero=E.numero
	AND Pr.rfc=E.rfc
	AND denominacion LIKE 'Televisa en acci�n'
)

CREATE VIEW Televisa AS
(
	SELECT denominacion, E.rfc, razonsocial
	FROM Proyectos P, Entregan E, Proveedores Pr
	WHERE P.numero=E.numero
	AND Pr.rfc=E.rfc
	AND denominacion LIKE 'Educando en Coahuila'
)

SELECT DISTINCT denominacion, E.rfc, razonsocial
FROM Proyectos P, Entregan E, Proveedores Pr
WHERE P.numero=E.numero
AND Pr.rfc=E.rfc
AND denominacion LIKE 'Televisa en acci�n'
AND RazonSocial NOT IN ( SELECT razonsocial
						 FROM Proyectos P, Entregan E, Proveedores Pr
						 WHERE P.numero=E.numero
						 AND Pr.rfc=E.rfc
						 AND denominacion LIKE 'Educando en Coahuila' )

CREATE VIEW ProveedoresTelevisa (denominacion, rfc, razonsocial) as
(
	select distinct pr.denominacion, p.rfc, p.RazonSocial
	from proveedores p, proyectos pr, entregan e
	where p.rfc = e.rfc AND pr.numero = e.numero AND pr.denominacion LIKE 'Televisa en acci�n'
)

CREATE VIEW ProveedoresCoahuila (denominacion, rfc, razonsocial) as
(
	select distinct pr.denominacion, p.rfc, p.RazonSocial
	from proveedores p, proyectos pr, entregan e
	where p.rfc = e.rfc AND pr.numero = e.numero AND pr.denominacion LIKE 'Educando en Coahuila'
)

CREATE VIEW diferenciaProveedores As
(
	select razonsocial from ProveedoresTelevisa
	except
	select razonsocial from ProveedoresCoahuila
)

SELECT * FROM ProveedoresTelevisa WHERE razonsocial IN (SELECT * FROM diferenciaProveedores)


select distinct m.descripcion, m.costo from materiales m, entregan e, ProveedoresTelevisa pt, ProveedoresCoahuila pc
where m.clave = e.clave AND e.rfc in (pt.rfc) AND e.rfc in (pc.rfc)

SELECT  Descripcion, count(Descripcion) as CantidadEntregada, SUM(Costo*Cantidad) as CostoTotal
FROM Entregan e, Materiales m
WHERE e.Clave = m.Clave
GROUP BY Descripcion

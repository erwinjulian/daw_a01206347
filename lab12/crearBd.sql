/* ERWIN JULIAN
A01206347 LAB 12 
22 DE SEPTIEMBRE*/

/* Eliminar Tablas creadas en el lab anterior */
DROP TABLE Materiales
DROP TABLE Entregan
DROP TABLE Proyectos
DROP TABLE Proveedores

/* CREACI�N DE TABLAS CON INSTRUCCI�N NOT NULL EN EL CAMPO LLAVE, SE EJECUTA SI LA TABLA EXISTE PARA NO SOBRE ESCRIBIRLA */
IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'Materiales')

DROP TABLE Materiales

CREATE TABLE Materiales
(
	Clave numeric (5) not null,
	Descripcion varchar(50),
	Costo numeric (8,2)
)

IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'Proveedores')

DROP TABLE Proveedores

CREATE TABLE Proveedores
(
	RFC char (13) not null,
	RazonSocial varchar(50)
)

IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'Proyectos')

DROP TABLE Proyectos

CREATE TABLE Proyectos
(
	Numero numeric (5) not null,
	Denominacion varchar(50)
)

IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'Entregan')

DROP TABLE Entregan

CREATE TABLE Entregan
(
	Clave numeric (5) not null,
	RFC char(13) not null,
	Numero numeric (5) not null,
	Fecha DateTime not null,
	Cantidad numeric (8,2)
)

SET DATEFORMAT dmy


/* Cargado de tablas*/

BULK INSERT a1206347.a1206347.[Entregan]
   FROM 'e:\wwwroot\a1206347\entregan.csv'
   WITH 
      (
         CODEPAGE = 'ACP',
         FIELDTERMINATOR = ',',
         ROWTERMINATOR = '0x0a'
      )

	  BULK INSERT a1206347.a1206347.[Materiales]
   FROM 'e:\wwwroot\a1206347\materiales.csv'
   WITH 
      (
         CODEPAGE = 'ACP',
         FIELDTERMINATOR = ',',
         ROWTERMINATOR = '0x0a'
      )

	  BULK INSERT a1206347.a1206347.[Proveedores]
   FROM 'e:\wwwroot\a1206347\proveedores.csv'
   WITH 
      (
         CODEPAGE = 'ACP',
         FIELDTERMINATOR = ',',
         ROWTERMINATOR = '0x0a'
      )


	  BULK INSERT a1206347.a1206347.[Proyectos]
   FROM 'e:\wwwroot\a1206347\proyectos.csv'
   WITH 
      (
         CODEPAGE = 'ACP',
         FIELDTERMINATOR = ',',
         ROWTERMINATOR = '0x0a'
      )


	  /* Consultar todos los registros de la tabla materiales */
SELECT * FROM Materiales

/* Insertar el registro a la tabla materiales */
INSERT INTO Materiales values (1000,'xxx',1000)

/* Eliminar el registro de la tabla materiales */
DELETE FROM Materiales where Clave=1000 and Costo=1000

/* Agregar llaves primarias */
ALTER TABLE Materiales add constraint llaveMateriales PRIMARY KEY (Clave)
ALTER TABLE Proveedores add constraint llaveProveedores PRIMARY KEY (RFC)
ALTER TABLE Proyectos add constraint llaveProyectos PRIMARY KEY (Numero)
ALTER TABLE Entregan add constraint llaveEntregan PRIMARY KEY (Clave,RFC,Numero,Fecha)

/* Checar las llaves primarias definidas */
sp_helpconstraint materiales
sp_helpconstraint proveedores
sp_helpconstraint proyectos
sp_helpconstraint entregan

/* En caso de cometer alg{un error, eliminar constraint */
ALTER TABLE Proveedores DROP constraint llaveProveedores

SELECT * FROM Materiales
SELECT * FROM Entregan
SELECT * FROM Proveedores
SELECT * FROM Proyectos

INSERT INTO Entregan values (0,'xxx',0,'1-jan-02',0);

DELETE FROM Entregan where Clave = 0

/* Agregar llaves for�neas */
ALTER TABLE Entregan add constraint cfentreganclave foreign key (Clave) references materiales (Clave)
ALTER TABLE Entregan add constraint rfentreganclave foreign key (RFC) references proveedores (RFC)
ALTER TABLE Entregan add constraint nfentreganclave foreign key (Numero) references proyectos (Numero)

ALTER TABLE Entregan add constraint cfentreganclave foreign key (Clave) references materiales (Clave)
ALTER TABLE Entregan add constraint rfentreganclave foreign key (RFC) references proveedores (RFC)
ALTER TABLE Entregan add constraint nfentreganclave foreign key (Numero) references proyectos (Numero)

sp_helpconstraint Entregan

INSERT INTO entregan values (1000,'AAAA800101',5000,GETDATE(),0);

DELETE FROM Entregan where Cantidad = 0

ALTER TABLE Entregan add constraint cantidad check (cantidad>0);


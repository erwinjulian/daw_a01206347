-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 26-10-2018 a las 20:19:20
-- Versión del servidor: 5.5.57-0ubuntu0.14.04.1
-- Versión de PHP: 5.5.9-1ubuntu4.22

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `Petz`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Zombie`
--

CREATE TABLE IF NOT EXISTS `Zombie` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre_completo` varchar(50) NOT NULL,
  `estado_actual` enum('infeccion','coma','transformacion','completamente_muerto') NOT NULL,
  `fecha_hora_registro` datetime NOT NULL,
  `fecha_hora_transicion` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=32 ;

--
-- Volcado de datos para la tabla `Zombie`
--

INSERT INTO `Zombie` (`id`, `nombre_completo`, `estado_actual`, `fecha_hora_registro`, `fecha_hora_transicion`) VALUES
(21, 'Eduardo Juárez', 'infeccion', '2018-10-26 02:49:27', '2018-10-26 02:49:27'),
(22, 'Erwin Julián', 'coma', '2018-10-26 02:49:40', '2018-10-26 02:49:40'),
(23, 'Ricardo Cortés', 'transformacion', '2018-10-26 02:49:54', '2018-10-26 02:49:54'),
(24, 'Paco Marín', 'completamente_muerto', '2018-10-26 02:50:09', '2018-10-26 02:50:09'),
(25, 'Luis Canales', 'coma', '2018-10-26 02:50:22', '2018-10-26 02:50:22'),
(26, 'Leco Díaz', 'infeccion', '2018-10-26 03:07:18', '2018-10-26 03:07:18'),
(27, 'Hockey Cortés', 'infeccion', '2018-10-26 03:07:31', '2018-10-26 03:07:31'),
(28, 'Rocio Aldeco', 'transformacion', '2018-10-26 03:07:41', '2018-10-26 03:07:41'),
(29, 'Pedro Óscar', 'completamente_muerto', '2018-10-26 03:07:55', '2018-10-26 03:07:55'),
(30, 'Andrés Löpez', 'coma', '2018-10-26 03:08:04', '2018-10-26 03:08:04'),
(31, 'Alan Macías', 'coma', '2018-10-26 03:08:16', '2018-10-26 03:08:16');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

<?php
    session_start();
    if(isset($_GET['estado'])){
        $valor = $_GET['estado'];
    }else
        $valor = 1;
    require_once("../Modelo/util.php"); //Para pedir el archivo sólo si no se ha incluido previamente
    require("../Vistas/_header.html");
    require("../Vistas/agregar.html");
    require("../Vistas/consultas.html");
    require("../Vistas/_footer.html");
    //Para desplegar un mensaje cuando se haya registrado correctamente o editado un registro
    if (isset($_SESSION["mensaje"])) {
        $mensaje = $_SESSION["mensaje"];
        require("../Vistas/mensaje.html");
        unset($_SESSION["mensaje"]);
    }
?>

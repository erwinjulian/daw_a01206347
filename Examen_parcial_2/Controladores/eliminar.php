<?php
    session_start();
    require_once("../Modelo/util.php");

    $registro = getRegistro(connectDb(), $_GET["id"]);
    delete($registro["id"]);
    $_SESSION["mensaje"] = 'El zombie '. $registro["nombre_completo"].' con estado ' . $registro["estado_actual"] . ' se eliminó correctamente';
    header("location:consultas.php");
?>
